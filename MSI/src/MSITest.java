import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MSITest {
    @Test
    void test0() {
        var msi = new MSI(new int[]{});
        assertEquals(0, msi.getMaxVal());
        assertTrue(msi.getOptSol().isEmpty());
    }

    @Test
    void test1() {
        var msi = new MSI(new int[]{});
        assertEquals(0, msi.getMaxVal());
        assertTrue(msi.getOptSol().isEmpty());
    }

    @Test
    void test2() {
        var msi = new MSI(new int[]{2, 3, 4, 5, 1, 7});
        assertEquals(15, msi.getMaxVal());
        var optSol = msi.getOptSol();
        assertEquals(3, optSol.size());
        assertTrue(optSol.contains(1));
        assertTrue(optSol.contains(3));
        assertTrue(optSol.contains(5));
    }

    @Test
    void test3() {
        var msi = new MSI(new int[]{3, 2, 5, 6, 1});
        assertEquals(9, msi.getMaxVal());
        var optSol = msi.getOptSol();
        assertEquals(2, optSol.size());
        assertTrue(optSol.contains(0));
        assertTrue(optSol.contains(3));
    }

    @Test
    void test4() {
        var msi = new MSI(new int[]{0, 2, 5, 6, 1});
        assertEquals(8, msi.getMaxVal());
        var optSol = msi.getOptSol();
        assertEquals(2, optSol.size());
        assertTrue(optSol.contains(1));
        assertTrue(optSol.contains(3));
    }
}