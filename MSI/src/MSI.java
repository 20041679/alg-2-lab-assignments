import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MSI {
    private int[] w;
    private int[] a;
    public MSI(int[] w) {
        this.w = w;
    }

    public int getMaxVal() {
        a = new int[w.length];
        if (w.length == 0) { return 0; }
        a[0] = w[0];
        if (w.length == 1) { return a[w.length - 1]; }

        a[1] = Integer.max(w[0], w[1]);
        for (int i = 2; i < w.length; ++i) {
            a[i] = Integer.max(a[i - 1], a[i - 2] + w[i]);
        }

        return a[w.length - 1];
    }

    public ArrayList<Integer> getOptSol() {
        getMaxVal();
        var s = new ArrayList<Integer>();
        var i = w.length - 1;
        while (i > 0) {
            if (a[i] > a[i - 1]) {
                s.add(i);
                i -= 2;
            } else {
                i -= 1;
            }
        }
        if (i == 0) {
            s.add(0);
        }
        return s;
    }
}
