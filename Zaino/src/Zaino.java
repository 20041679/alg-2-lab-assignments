import it.uniupo.algoTools.MaxHeap;

public class Zaino {
    private int[] values;
    private int[] volumes;
    private int capacity;
    public Zaino(int[] values, int[] volumes, int capacity) {
        this.values = values;
        this.volumes = volumes;
        this.capacity = capacity;
    }

    public int getMaxVal() {
        int val[][] = new int[values.length][capacity + 1];
        for (int cap = 0; cap <= capacity; cap++) {
            val[0][cap] = cap >= volumes[0] ? values[0] : 0;
        }
        for (int objNum = 1; objNum < values.length; objNum++) {
            for (int cap = 1; cap <= capacity; cap++) {
                val[objNum][cap] = volumes[objNum] <= cap
                        ? Integer.max(
                            val[objNum - 1][cap - volumes[objNum]] + values[objNum],
                            val[objNum - 1][cap]
                        )
                        : val[objNum - 1][cap];
            }
        }
        return val[values.length - 1][capacity];
    }

    public int getMaxValApprox() {
        MaxHeap<Integer, Double> heap = new MaxHeap<>();
        for (int i = 0; i < values.length; i++) {
            heap.add(i, (double)values[i] / volumes[i]);
        }
        int ret = 0;
        int capacity = this.capacity;
        while (capacity > 0) {
            Integer best = nextFit(capacity, heap);
            if (best != null) {
                capacity -= volumes[best];
                ret += values[best];
            } else {
                break;
            }
        }
        return ret;
    }

    private Integer nextFit(int capacity, MaxHeap<Integer, Double> heap) {
        while (!heap.isEmpty()) {
            int best = heap.extractMax();
            if (capacity >= volumes[best]) {
                return best;
            }
        }
        return null;
    }
}
