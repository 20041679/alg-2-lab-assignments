import it.uniupo.graphLib.InOut;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ZainoTest {
    private static final String PATH = "instances_01_KP/";

    @Test
    public void userTest() {
        int capacity;
        int[] volumes;
        volumes = new int[] { 1, 3, 2 };
        int[] values;
        values = new int[] { 2, 3, 1 };
        assertEquals(3, (new Zaino(values, volumes, 3)).getMaxVal());
        volumes = new int[]{ 1, 3, 2 };
        values = new int[]{ 2, 3, 2 };
        assertEquals(4, (new Zaino(values, volumes, 3)).getMaxVal());
    }

    @Test
    public void testEasypeasy() {
        String typename = "low-dimensional";
        String filename = "f1_l-d_kp_10_269";
        String[] instance = InOut.readStringArray(PATH+typename+"/"+filename);
        String[] params = instance[0].split(" ");
        int n = Integer.parseInt(params[0]);
        int cap = Integer.parseInt(params[1]);
        int[] values = new int[n];
        int[] vols = new int[n];
        for (int i = 0; i < n; i++) {
            String[] pieces = instance[i+1].split(" ");
            values[i] = Integer.parseInt(pieces[0]);
            vols[i] = Integer.parseInt(pieces[1]);
        }
        Zaino zai = new Zaino(values,vols, cap);
        int maxVal = Integer.parseInt(InOut.readString(PATH+typename+"-optimum/"+filename));
        assertEquals(maxVal,zai.getMaxVal());
    }

    @Test
    public void test1() {
        String typename = "large_scale";
        String filename = "knapPI_1_100_1000_1";
        String[] instance = InOut.readStringArray(PATH+typename+"/"+filename);
        String[] params = instance[0].split(" ");
        int n = Integer.parseInt(params[0]);
        int cap = Integer.parseInt(params[1]);
        int[] values = new int[n];
        int[] vols = new int[n];
        for (int i = 0; i < n; i++) {
            String[] pieces = instance[i+1].split(" ");
            values[i] = Integer.parseInt(pieces[0]);
            vols[i] = Integer.parseInt(pieces[1]);
        }
        Zaino zai = new Zaino(values,vols, cap);
        int maxVal = Integer.parseInt(InOut.readString(PATH+typename+"-optimum/"+filename));
        assertEquals(maxVal,zai.getMaxVal());
    }

    @Test
    public void test2() {
        String typename = "large_scale";
        String filename = "knapPI_3_100_1000_1";
        String[] instance = InOut.readStringArray(PATH+typename+"/"+filename);
        String[] params = instance[0].split(" ");
        int n = Integer.parseInt(params[0]);
        int cap = Integer.parseInt(params[1]);
        int[] values = new int[n];
        int[] vols = new int[n];
        for (int i = 0; i < n; i++) {
            String[] pieces = instance[i+1].split(" ");
            values[i] = Integer.parseInt(pieces[0]);
            vols[i] = Integer.parseInt(pieces[1]);
        }
        Zaino zai = new Zaino(values,vols, cap);
        int maxVal = Integer.parseInt(InOut.readString(PATH+typename+"-optimum/"+filename));
        assertEquals(maxVal,zai.getMaxVal());
    }

    @Test
    public void testEasypeasyApprox() {
        String typename = "low-dimensional";
        String filename = "f1_l-d_kp_10_269";
        String[] instance = InOut.readStringArray(PATH+typename+"/"+filename);
        String[] params = instance[0].split(" ");
        int n = Integer.parseInt(params[0]);
        int cap = Integer.parseInt(params[1]);
        int[] values = new int[n];
        int[] vols = new int[n];
        for (int i = 0; i < n; i++) {
            String[] pieces = instance[i+1].split(" ");
            values[i] = Integer.parseInt(pieces[0]);
            vols[i] = Integer.parseInt(pieces[1]);
        }
        Zaino zai = new Zaino(values,vols, cap);
        int maxVal = Integer.parseInt(InOut.readString(PATH+typename+"-optimum/"+filename));
        assertEquals(maxVal,zai.getMaxVal());
        assertTrue(zai.getMaxValApprox() >= zai.getMaxVal() / 2);
    }

    @Test
    public void test1Approx() {
        String typename = "large_scale";
        String filename = "knapPI_1_100_1000_1";
        String[] instance = InOut.readStringArray(PATH+typename+"/"+filename);
        String[] params = instance[0].split(" ");
        int n = Integer.parseInt(params[0]);
        int cap = Integer.parseInt(params[1]);
        int[] values = new int[n];
        int[] vols = new int[n];
        for (int i = 0; i < n; i++) {
            String[] pieces = instance[i+1].split(" ");
            values[i] = Integer.parseInt(pieces[0]);
            vols[i] = Integer.parseInt(pieces[1]);
        }
        Zaino zai = new Zaino(values,vols, cap);
        int maxVal = Integer.parseInt(InOut.readString(PATH+typename+"-optimum/"+filename));
        assertEquals(maxVal,zai.getMaxVal());
        assertTrue(zai.getMaxValApprox() >= zai.getMaxVal() / 2);
    }

    @Test
    public void test2Approx() {
        String typename = "large_scale";
        String filename = "knapPI_3_100_1000_1";
        String[] instance = InOut.readStringArray(PATH+typename+"/"+filename);
        String[] params = instance[0].split(" ");
        int n = Integer.parseInt(params[0]);
        int cap = Integer.parseInt(params[1]);
        int[] values = new int[n];
        int[] vols = new int[n];
        for (int i = 0; i < n; i++) {
            String[] pieces = instance[i+1].split(" ");
            values[i] = Integer.parseInt(pieces[0]);
            vols[i] = Integer.parseInt(pieces[1]);
        }
        Zaino zai = new Zaino(values,vols, cap);
        int maxVal = Integer.parseInt(InOut.readString(PATH+typename+"-optimum/"+filename));
        assertEquals(maxVal,zai.getMaxVal());
        assertTrue(zai.getMaxValApprox() >= zai.getMaxVal() / 2);
    }
}