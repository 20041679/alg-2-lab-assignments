import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DFS {
    GraphInterface g;
    GraphInterface tree;
    List<Integer> visited;
    public DFS(GraphInterface g) {
        this.g = g;
    }

    int[] fathers;

    public GraphInterface getTree(int source) {
        if (source < 0 || source >= g.getOrder()) {
            throw new IllegalArgumentException("invalid source");
        }
        tree = g.create();
        visited = new ArrayList<>();
        visit(source);
        return tree;
    }

    private void visit(int source) {
        visited.add(source);
        for (Integer neighbour : g.getNeighbors(source)) {
            if (!visited.contains(neighbour)) {
                tree.addEdge(source, neighbour);
                visit(neighbour);
            }
        }
    }

    public GraphInterface getForest() {
        tree = g.create();
        visited = new ArrayList<>();
        for (int i = 0; i < g.getOrder(); i++) {
            if (!visited.contains(i)) {
                visit(i);
            }
        }
        return tree;
    }

    public GraphInterface getTreeRicoprente(int source) throws NotAllNodesReachedException {
        GraphInterface tree = getTree(source);
        if (tree.getEdgeNum() < g.getOrder() - 1) {
            throw new NotAllNodesReachedException();
        }
        return tree;
    }

    private void completeVisit() {
        tree = g.create();
        visited = new ArrayList<>();
        for (int i = 0; i < g.getOrder(); i++) {
            if (!visited.contains(i)) {
                visit(i);
            }
        }
    }

    public boolean hasUndirectedCycle() {
        if (g instanceof UndirectedGraph) {
            return _hasUndirectedCycle();
        } else {
            return false;
        }
    }

    private boolean _hasUndirectedCycle() {
        for (int source = 0; source < g.getOrder(); source++) {
            fathers = new int[g.getOrder()];
            fathers[source] = -1;
            visited = new ArrayList<>();
            if(_hasUndirectedCycle(source)) {
                return true;
            }
        }
        return false;
    }

    private boolean _hasUndirectedCycle(int source) {
        visited.add(source);
        for (Integer neighbour : g.getNeighbors(source)) {
            if (!visited.contains(neighbour)) {
                fathers[neighbour] = source;
                if (_hasUndirectedCycle(neighbour)) {
                    return true;
                }
            } else if (fathers[source] != -1 && neighbour != fathers[source]) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Integer> getNodesInOrderPostVisit(int source) {
        visited = new ArrayList<>();
        return getNodesInOrderPostVisit(source, new ArrayList<>());
    }


    public ArrayList<Integer> getNodesInOrderPostVisit(int source, ArrayList<Integer> ret) {
        visited.add(source);
        for (Integer neighbour : g.getNeighbors(source)) {
            if (!visited.contains(neighbour)) {
                getNodesInOrderPostVisit(neighbour, ret);
            }
        }
        ret.add(source);
        return ret;
    }

    public boolean hasDirectedCycle() {
        if (g instanceof DirectedGraph) {
            return _hasUndirectedCycle();
        } else {
            return false;
        }
    }

    public ArrayList<Integer> getDirCycle() {
        for (int source = 0; source < g.getOrder(); source++) {
            fathers = new int[g.getOrder()];
            fathers[source] = -1;
            visited = new ArrayList<>();
            ArrayList<Integer> ret = getDirCycle(source);
            if (ret != null) {
                return ret;
            }
        }
        return null;
    }

    private ArrayList<Integer> getDirCycle(int source) {
        visited.add(source);
        for (Integer neighbour : g.getNeighbors(source)) {
            if (!visited.contains(neighbour)) {
                fathers[neighbour] = source;
                ArrayList<Integer> ret = getDirCycle(neighbour);
                if (ret != null) {
                    return ret;
                }
            } else if (fathers[source] != -1 && neighbour != fathers[source]) {
                return getCycle(source, neighbour);
            }
        }
        return null;
    }

    private ArrayList<Integer> getCycle(int to, int from) {
        ArrayList<Integer> ret = new ArrayList<>();
        while (to != from) {
            ret.add(0, to);
            to = fathers[to];
        }
        return ret;
    }
}
