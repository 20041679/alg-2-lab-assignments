import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.GraphInterface;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BFSTest {
    @Test
    void getShortestPath() {
        GraphInterface g = new DirectedGraph("5; 3 4; 4 2; 2 3; 4 1; 1 2; 1 0");
        ArrayList<Edge> expected = new ArrayList<>(List.of((new Edge(3, 4)), (new Edge(4, 2))));
        ArrayList<Edge> got = (new BFS(g)).getShortestPath(3, 2);
        Assertions.assertEquals(expected, got);
        expected = new ArrayList<>(List.of((new Edge(3, 4)), (new Edge(4, 1)), (new Edge(1, 0))));
        got = (new BFS(g)).getShortestPath(3, 0);
        Assertions.assertEquals(expected, got);
    }
}