import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.GraphInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class BFS {
    private GraphInterface g;
    private int[] fathers;

    public BFS(GraphInterface g) {
        this.g = g;
    }

    public ArrayList<Integer> getNodesInOrderOfVisit(int source) {
        Queue<Integer> queue = new LinkedList<>();
        boolean[] discovered = new boolean[g.getOrder()];

        ArrayList<Integer> ret = new ArrayList<>();
        queue.add(source);
        discovered[source] = true;
        while (!queue.isEmpty()) {
            Integer u = queue.remove();
            ret.add(u);
            for (Integer v : g.getNeighbors(u)) {
                if (!discovered[v]) {
                    queue.add(v);
                    discovered[v] = true;
                }
            }
        }
        return ret;
    }

    public int[] getDistance(int source) {
        Queue<Integer> queue = new LinkedList<>();
        boolean[] discovered = new boolean[g.getOrder()];
        int[] distances = new int[g.getOrder()];
        Arrays.fill(distances, -1);
        distances[source] = 0;

        queue.add(source);
        discovered[source] = true;
        while (!queue.isEmpty()) {
            Integer u = queue.remove();
            for (Integer v : g.getNeighbors(u)) {
                if (!discovered[v]) {
                    queue.add(v);
                    discovered[v] = true;
                    distances[v] = distances[u] + 1;
                }
            }
        }

        return distances;
    }

    public int[] getWeightedDistance(int source) {
        Queue<Integer> queue = new LinkedList<>();
        boolean[] discovered = new boolean[g.getOrder()];
        int[] distances = new int[g.getOrder()];
        Arrays.fill(distances, -1);
        distances[source] = 0;

        queue.add(source);
        discovered[source] = true;
        while (!queue.isEmpty()) {
            Integer u = queue.remove();
            for (Edge uv : g.getOutEdges(u)) {
                int v = uv.getHead();
                if (!discovered[v]) {
                    queue.add(v);
                    discovered[v] = true;
                    distances[v] = distances[u] + uv.getWeight();
                }
            }
        }

        return distances;
    }

    public ArrayList<Edge> getShortestPath(int source, int dest) {
        makeFathers(source);
        ArrayList<Edge> ret = new ArrayList<>();
        int tmp = dest;
        while (fathers[tmp] != -1) {
            ret.add(0, new Edge(fathers[tmp], tmp));
            tmp = fathers[tmp];
        }
        return ret;
    }


    private void makeFathers(int source) {
        fathers = new int[g.getOrder()];
        fathers[source] = -1;
        Queue<Integer> queue = new LinkedList<>();
        boolean[] discovered = new boolean[g.getOrder()];
        discovered[source] = true;

        queue.add(source);
        while (!queue.isEmpty()) {
            int node = queue.remove();
            for (Integer neighbour : g.getNeighbors(node)) {
                if (!discovered[neighbour]) {
                    discovered[neighbour] = true;
                    queue.add(neighbour);
                    fathers[neighbour] = node;
                }
            }
        }
    }
}
