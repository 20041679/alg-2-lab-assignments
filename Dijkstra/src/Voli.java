import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.algoTools.MinHeap;
import it.uniupo.graphLib.Edge;

import java.util.ArrayList;
import java.util.Arrays;

public class Voli {
    int[] d;
    boolean[] visited;
    MinHeap<Edge, Integer> h;
    DirectedGraph g;

    public Voli(DirectedGraph collegamenti) {
        g = collegamenti;
        d = new int[collegamenti.getOrder()];
        h = new MinHeap<>();
        visited = new boolean[g.getOrder()];
    }

    private void init(int source) {
        Arrays.fill(d, -1);
        d[source] = 0;
        for (Edge su : g.getOutEdges(source)) {
            h.add(su, d[source] + su.getWeight());
        }
        visited[source] = true;
    }

    private void validate(int partenza, int destinazione) {
        if (partenza < 0 || destinazione < 0 || partenza > g.getOrder() || destinazione > g.getOrder()) {
            throw new IllegalArgumentException();
        }
    }

    int tempoMinimo(int partenza, int destinazione) {
        validate(partenza, destinazione);
        init(partenza);
        while (!h.isEmpty()) {
            Edge uw = h.extractMin();
            int u = uw.getTail();
            int w = uw.getHead();
            if (!visited[w]) {
                visited[w] = true;
                d[w] = d[u] + uw.getWeight();
                for (Edge wz : g.getOutEdges(w)) {
                    int z = wz.getHead();
                    if (!visited[z]) {
                        h.add(wz, d[w] + wz.getWeight());
                    }
                }
            }
        }
        return d[destinazione];
    }

    int scali(int partenza, int destinazione) {
        validate(partenza, destinazione);
        int ret = (new BFS(g)).getDistance(partenza)[destinazione];
        return ret == -1 ? -1 : ret - 1;
    }

    int tempo (int partenza, int destinazione) {
        validate(partenza, destinazione);
        return (new BFS(g)).getWeightedDistance(partenza)[destinazione];
    }

    ArrayList<Edge> trattaVeloce (int partenza, int destinazione) {
        return null;
    }

    ArrayList<Integer> elencoScali(int partenza, int destinazione) {
        return null;
    }
}
