package src;

import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MSTTest {
    MST mstPrim = new Prim(new UndirectedGraph("1"));
    MST mstKruskal = new Kruskal(new UndirectedGraph("1"));

    @Test
    public void testCreatePrim() {
        UndirectedGraph nuovoGrafo = new UndirectedGraph("2\n1 0 2");
        MST testMST = mstPrim.create(nuovoGrafo);
        assertNotNull(testMST);
    }

    @Test
    public void testPrim() {
        UndirectedGraph nuovoGrafo = new UndirectedGraph("2\n1 0 2");
        MST testMST = mstPrim.create(nuovoGrafo);
        UndirectedGraph tree = testMST.getMST();
        assertTrue(tree.hasEdge(1, 0));
        assertEquals(2, testMST.getCost());
    }

    @Test
    public void testCreateKruskal() {
        UndirectedGraph nuovoGrafo = new UndirectedGraph("2\n1 0 2");
        MST testMST = mstKruskal.create(nuovoGrafo);
        assertNotNull(testMST);
    }

    @Test
    public void testKruskal() {
        UndirectedGraph nuovoGrafo = new UndirectedGraph("2\n1 0 2");
        MST testMST = mstKruskal.create(nuovoGrafo);
        UndirectedGraph tree = testMST.getMST();
        assertTrue(tree.hasEdge(1, 0));
        assertEquals(2, testMST.getCost());
    }
}