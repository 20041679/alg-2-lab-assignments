package src;

import it.uniupo.graphLib.UndirectedGraph;

public interface MST {
    public abstract MST create(UndirectedGraph g);
    public abstract UndirectedGraph getMST();
    public abstract int getCost();
}
