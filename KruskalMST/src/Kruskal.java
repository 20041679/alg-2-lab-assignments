package src;

import it.uniupo.algoTools.MinHeap;
import it.uniupo.algoTools.UnionByRank;
import it.uniupo.algoTools.UnionFind;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.UndirectedGraph;

public class Kruskal implements MST {
    private UndirectedGraph g;
    private Integer cost;

    public Kruskal(UndirectedGraph g) {
        this.g = g;
        cost = null;
    }
    @Override
    public MST create(UndirectedGraph g) {
        return new Kruskal(g);
    }

    @Override
    public UndirectedGraph getMST() {
        if (cost == null) {
            cost = 0;
        }
        UndirectedGraph tree = (UndirectedGraph) g.create();
        MinHeap<Edge, Integer> heap = new MinHeap<>();
        for (int u = 0; u < tree.getOrder(); ++u) {
            for (Edge e : g.getOutEdges(u)) {
                heap.add(e, e.getWeight());
            }
        }
        UnionFind uf = new UnionByRank(g.getOrder());
        while (!heap.isEmpty()) {
            Edge uv = heap.extractMin();
            int u = uv.getTail();
            int v = uv.getHead();
            int findU = uf.find(u);
            int findV = uf.find(v);
            if (findU != findV) {
                tree.addEdge(uv);
                cost += uv.getWeight();
                uf.union(findU, findV);
            }
        }
        return tree;
    }

    @Override
    public int getCost() {
        if (cost == null) {
            getMST();
            return cost;
        } else {
            return cost;
        }
    }
}
