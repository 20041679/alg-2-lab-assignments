package src;

import it.uniupo.algoTools.MinHeap;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;

public class Prim implements MST{
    private UndirectedGraph g;
    private int cost;

    Prim(GraphInterface g) {
        this.g = (UndirectedGraph) g;
    }

    private UndirectedGraph visit(int source) {
        cost = 0;
        boolean[] visited = new boolean[g.getOrder()];
        visited[source] = true;
        MinHeap<Edge, Integer> heap = new MinHeap<>();
        UndirectedGraph tree = (UndirectedGraph) g.create();
        for (Edge e : g.getOutEdges(source)) {
            heap.add(e, e.getWeight());
        }
        while (!heap.isEmpty()) {

            Edge uv = heap.extractMin();
            // System.out.println("heap not empty, edge" + uv);
            int v = uv.getHead();
            if (!visited[v]) {
                // System.out.println(v + " not visited");
                visited[v] = true;
                tree.addEdge(uv);
                cost += uv.getWeight();
                // System.out.println("cost " + cost + " += " + uv.getWeight());
                for (Edge vz : g.getOutEdges(v)) {
                    int z = vz.getHead();
                    if (!visited[z]) {
                        heap.add(vz, vz.getWeight());
                    }
                }
            }
        }
        return tree;
    }

    @Override
    public MST create(UndirectedGraph g) {
        return new Prim(g);
    }

    public UndirectedGraph getMST() {
        return visit(0);
    }

    public int getCost() {
        visit(0);
        return this.cost;
    }
}

