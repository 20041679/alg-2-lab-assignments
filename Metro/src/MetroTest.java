import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MetroTest {
    @Test
    void testCreate() {
        assertNotNull((new Metro(new UndirectedGraph("1"))));
    }

    @Test
    void test1() {
        assertTrue((new Metro(new UndirectedGraph("1"))).fermate(0, 0).isEmpty());
    }

    @Test
    void test2() {
        var path = (new Metro(new UndirectedGraph("2; 0 1"))).fermate(0, 1);
        assertEquals(1, path.size());
        assertEquals(1, path.getLast());
    }

    @Test
    void test3() {
        var path = (new Metro(new UndirectedGraph("3; 0 1; 1 2"))).fermate(0, 2);
        assertEquals(2, path.size());
        assertEquals(1, path.getFirst());
        assertEquals(2, path.getLast());
    }

    @Test
    void test32() {
        var path = (new Metro(new UndirectedGraph("3; 0 1; 0 2; 2 1"))).fermate(0, 1);
        assertEquals(1, path.size());
        assertEquals(1, path.getLast());
    }
}