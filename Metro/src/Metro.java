import it.uniupo.graphLib.UndirectedGraph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Metro {
    private UndirectedGraph mappa;
    private boolean[] visited;

    public Metro(UndirectedGraph mappa) {
        this.mappa = mappa;
        visited = new boolean[mappa.getOrder()];
    }

    private void validateNode(int node) {
        if (node < 0 || node >= mappa.getOrder()) {
            throw new IllegalArgumentException();
        }
    }

    public ArrayList<Integer> fermate(int stazPartenza, int stazArrivo) {
        validateNode(stazPartenza);
        validateNode(stazArrivo);
        var fathers = new int[mappa.getOrder()];
        var queue = new LinkedList<Integer>();
        queue.add(stazPartenza);
        while (!queue.isEmpty()) {
            var u = queue.pop();
            visited[u] = true;
            if (u == stazArrivo) {
                return backTrack(stazPartenza, stazArrivo, fathers);
            }
            for (var v : mappa.getNeighbors(u)) {
                if (!visited[v]) {
                    fathers[v] = u;
                    queue.add(v);
                }
            }
        }
        return null;
    }

    private ArrayList<Integer> backTrack(int source, int end, int[] fathers) {
        var ret = new ArrayList<Integer>();
        while (end != source) {
            ret.addFirst(end);
            end = fathers[end];
        }
        return ret;
    }
}
