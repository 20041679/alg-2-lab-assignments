import it.uniupo.algoTools.MaxHeap;

public class FracKnapsack {
    private double capacity;
    private double[] volume;
    private double[] value;
    private double[] dose;

    public FracKnapsack(double capacity, double[] volume, double[] value) {
        this.capacity = capacity;
        this.volume = volume;
        this.value = value;
        dose = new double[volume.length];
    }

    public double maxVal() {
        MaxHeap<Integer, Double> heap = new MaxHeap<>();
        for (int i = 0; i < volume.length; i++) {
            heap.add(i, value[i] / volume[i]);
        }
        double ret = 0;
        double left = capacity;
        while (left > 0 && !heap.isEmpty()) {
            int m = heap.extractMax();
            dose[m] = left >= volume[m] ? 1 : left / volume[m];
            ret += value[m] * dose[m];
            left -= dose[m] * volume[m];
        }
        return ret;
    }

    private void validate(int i) {
        if (i < 0 || i >= volume.length) {
            throw new IllegalArgumentException();
        }
    }

    public double dose(int i) {
        validate(i);
        return dose[i];
    }

    private double quant(int i) {
        return volume[i] * dose[i];
    }

    public boolean more(int i, int j) {
        validate(i);
        validate(j);
        return quant(i) >= quant(j);
    }
}
