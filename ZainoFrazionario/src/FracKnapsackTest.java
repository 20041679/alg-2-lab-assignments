import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FracKnapsackTest {
    @Test
    void testCreate() {
        assertNotNull((new FracKnapsack(0, new double[]{}, new double[]{})));
    }

    void testException() {
        var f = new FracKnapsack(7, new double[]{3}, new double[]{10});
        assertThrows(Exception.class, () -> f.dose(-1));
        assertThrows(Exception.class, () -> f.dose(1));
    }

    @Test
    void test11() {
        var f = new FracKnapsack(7, new double[]{3}, new double[]{10});
        assertEquals(10, f.maxVal());
        assertEquals(1, f.dose(0));
    }

    @Test
    void test12() {
        var f = new FracKnapsack(2, new double[]{4}, new double[]{10});
        assertEquals(5, f.maxVal());
        assertEquals(.5, f.dose(0));
    }

    @Test
    void test31() {
        var f = new FracKnapsack(10, new double[]{3, 2, 4}, new double[]{2, 5, 1});
        assertEquals(8, f.maxVal());
        assertEquals(1, f.dose(0));
        assertEquals(1, f.dose(1));
        assertEquals(1, f.dose(2));
    }

    @Test
    void test32() {
        var f = new FracKnapsack(3, new double[]{70, 12, 3}, new double[]{10, 2, .5});
        assertEquals(.5, f.maxVal());
        assertEquals(0, f.dose(0));
        assertTrue(0 == f.dose(1) || .25 == f.dose(1));
        assertTrue(0 == f.dose(2) || 1 == f.dose(2));
    }

    @Test
    void test33() {
        var f = new FracKnapsack(10, new double[]{4, 3, 4}, new double[]{5, 3, 1});
        assertEquals(8.75, f.maxVal());
        assertEquals(1, f.dose(0));
        assertEquals(1, f.dose(1));
        assertEquals(.75, f.dose(2));
    }
}