import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.GraphUtils;

import java.util.ArrayList;

public class Kosaraju {
    private DirectedGraph g;
    private DirectedGraph gt;
    private DFS dfs;

    public Kosaraju(DirectedGraph g) {
        this.g = g;
    }

    public ArrayList<Integer> ofvInverso() {
        ArrayList<Integer> ret = new ArrayList<>();
        dfs = new DFS(g);
        for (int i = 0; i < g.getOrder(); i++) {
            if (!dfs.isVisited(i)) {
                for (int v : dfs.getNodesInOrderPostVisit(i)) {
                    ret.addFirst(v);
                }
            }
        }
        return ret;
    }

    public int[] getSCC() {
        int[] scc = new int[g.getOrder()];
        int count = 0;
        ArrayList<Integer> inv = ofvInverso();
        gt = GraphUtils.reverseGraph(g);
        dfs = new DFS(gt);
        for (Integer i : inv) {
            if (!dfs.isVisited(i)) {
                getScc(i, count, scc);
                count++;
            }
        }
        return scc;
    }

    private void getScc(int source, int count, int[] scc) {
        if (!dfs.isVisited(source)) {
            dfs.setVisited(source);
            scc[source] = count;
            for (Integer neighbour : gt.getNeighbors(source)) {
                getScc(neighbour, count, scc);
            }
        }
    }
}
