import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.Graph;
import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DFSTest {
    @Test
    void testCycle() {
        Graph g = new UndirectedGraph("4; 1 0; 0 2; 2 3; 3 0");
        Assertions.assertTrue((new DFS(g)).hasUndirectedCycle());
        g = new UndirectedGraph("5; 0 1; 1 2; 2 3; 3 4; 4 0");
        Assertions.assertTrue((new DFS(g)).hasUndirectedCycle());
    }

    @Test
    void testNoCycle() {
        Graph g = new UndirectedGraph("3; 0 1; 0 2");
        Assertions.assertFalse((new DFS(g)).hasUndirectedCycle());
    }

    @Test
    void testNodesOrderPostVisit() {
        Graph g = new UndirectedGraph("4; 2 0; 1 0; 3 2; 3 1");
        ArrayList<Integer> order = (new DFS(g)).getNodesInOrderPostVisit(0);
        ArrayList<Integer> a = new ArrayList<>(List.of(1, 3, 2, 0));
        ArrayList<Integer> b = new ArrayList<>(List.of(2, 1, 3, 0));
        Assertions.assertTrue(order.equals(a) || order.equals(b));
    }

    @Test
    void hasUndirectedCycle() {
        Graph g = new UndirectedGraph("4; 1 0; 0 2; 2 3; 3 0");
        Assertions.assertTrue((new DFS(g)).hasUndirectedCycle());
        g = new UndirectedGraph("4; 1 0; 2 0; 3 0");
        Assertions.assertFalse((new DFS(g)).hasUndirectedCycle());
    }

    @Test
    void hasDirectedCycle() {
        Graph g = new DirectedGraph("1;");
        Assertions.assertFalse((new DFS(g)).hasDirectedCycle());
        g = new DirectedGraph("2; 0 1");
        Assertions.assertFalse((new DFS(g)).hasDirectedCycle());
        g = new DirectedGraph("3; 1 0; 1 2; 0 2");
        Assertions.assertFalse((new DFS(g)).hasDirectedCycle());
        g = new DirectedGraph("3; 0 2; 2 1; 1 0");
        Assertions.assertTrue((new DFS(g)).hasDirectedCycle());
        g = new DirectedGraph("5; 0 4; 4 1; 4 2; 3 4; 2 3");
        Assertions.assertTrue((new DFS(g)).hasDirectedCycle());
    }

    @Test
    void getDirCycle() {
        Graph g = new DirectedGraph("1;");
        System.out.println((new DFS(g)).getDirCycle());
    }

    @Test
    void isConnected() {
        Assertions.assertTrue((new DFS(new UndirectedGraph("1;"))).isConnected());
        Assertions.assertTrue((new DFS(new UndirectedGraph("2; 0 1"))).isConnected());
        Assertions.assertFalse((new DFS(new UndirectedGraph("2;"))).isConnected());
    }

    @Test
    void connectedComponents() {
        int[] cc;
        cc = (new DFS(new UndirectedGraph("1;"))).connectedComponents();
        Assertions.assertEquals(0, cc[0]);
        cc = (new DFS(new UndirectedGraph("2; 0 1"))).connectedComponents();
        Assertions.assertEquals(cc[0], cc[1]);
        cc = (new DFS(new UndirectedGraph("2;"))).connectedComponents();
        Assertions.assertNotEquals(cc[0], cc[1]);
        cc = (new DFS(new UndirectedGraph("6; 1 0; 0 4; 2 5"))).connectedComponents();
        Assertions.assertEquals(cc[0], cc[4]);
        Assertions.assertEquals(cc[4], cc[1]);
        Assertions.assertEquals(cc[2], cc[5]);
        Assertions.assertNotEquals(cc[4], cc[3]);
        Assertions.assertNotEquals(cc[4], cc[2]);
        Assertions.assertNotEquals(cc[3], cc[2]);
    }

    @Test
    void topologicalOrder() {
        ArrayList<Integer> ret;
        ret = (new DFS(new DirectedGraph("1;"))).topologicalOrder();
        Assertions.assertEquals(0, ret.indexOf(0));
        ret = (new DFS(new DirectedGraph("2; 0 1"))).topologicalOrder();
        Assertions.assertEquals(0, ret.indexOf(0));
        Assertions.assertEquals(1, ret.indexOf(1));
        ret = (new DFS(new DirectedGraph("3; 2 0"))).topologicalOrder();
        Assertions.assertTrue(ret.indexOf(2) < ret.indexOf(0));
        Assertions.assertTrue(ret.contains(1));
        ret = (new DFS(new DirectedGraph("4; 0 1; 0 2; 1 3; 2 3"))).topologicalOrder();
        Assertions.assertTrue(ret.indexOf(0) < ret.indexOf(1));
        Assertions.assertTrue(ret.indexOf(1) < ret.indexOf(3));
        Assertions.assertTrue(ret.indexOf(2) < ret.indexOf(3));
    }
}