import it.uniupo.graphLib.DirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

class KosarajuTest {
    @Test
    void ofvInverso() {
       ArrayList<Integer> ret;
       ret = new Kosaraju(new DirectedGraph("5; 1 4; 4 3; 3 1; 1 2; 4 0; 2 0; 0 2;")).ofvInverso();
       Assertions.assertTrue((ret.indexOf(1) < ret.indexOf(0)) && (ret.indexOf(1) < ret.indexOf(2)) ||
                (ret.indexOf(4) < ret.indexOf(0) && ret.indexOf(4) < ret.indexOf(2)) ||
                (ret.indexOf(3) < ret.indexOf(0) && ret.indexOf(3) < ret.indexOf(2)));
       ret = new Kosaraju(new DirectedGraph("5; 0 4; 4 3; 3 0; 0 2; 4 1; 2 1; 1 2;")).ofvInverso();
       Assertions.assertTrue((ret.indexOf(0) < ret.indexOf(1)) && (ret.indexOf(0) < ret.indexOf(2)) ||
               (ret.indexOf(4) < ret.indexOf(1) && ret.indexOf(4) < ret.indexOf(2)) ||
               (ret.indexOf(3) < ret.indexOf(1) && ret.indexOf(3) < ret.indexOf(2)));
    }

    @Test
    void getScc() {
        int[] ret;
        ret = new Kosaraju(new DirectedGraph("5; 1 4; 4 3; 3 1; 1 2; 4 0; 2 0; 0 2;")).getSCC();
        System.out.println(Arrays.toString(ret));
        Assertions.assertEquals(ret[0], ret[2]);
        Assertions.assertEquals(ret[1], ret[3]);
        Assertions.assertEquals(ret[3], ret[4]);
        Assertions.assertNotEquals(ret[0], ret[1]);
    }
}