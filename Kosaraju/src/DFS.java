import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DFS {
    GraphInterface g;
    GraphInterface tree;
    List<Integer> visited;

    public DFS(GraphInterface g) {
        this.g = g;
        resetVisited();
    }

    int[] fathers;

    public void resetVisited() {
        this.visited = new ArrayList<>();
    }

    public boolean isVisited(int i) {
        if (visited != null) {
            return visited.contains(i);
        }
        return false;
    }

    public void setVisited(int i) {
        visited.add(i);
    }

    public GraphInterface getTree(int source) {
        if (source < 0 || source >= g.getOrder()) {
            throw new IllegalArgumentException("invalid source");
        }
        tree = g.create();
        resetVisited();
        visit(source);
        return tree;
    }

    private void visit(int source) {
        visited.add(source);
        for (Integer neighbour : g.getNeighbors(source)) {
            if (!visited.contains(neighbour)) {
                tree.addEdge(source, neighbour);
                visit(neighbour);
            }
        }
    }

    public GraphInterface getForest() {
        tree = g.create();
        resetVisited();
        for (int i = 0; i < g.getOrder(); i++) {
            if (!visited.contains(i)) {
                visit(i);
            }
        }
        return tree;
    }

    public GraphInterface getTreeRicoprente(int source) throws NotAllNodesReachedException {
        GraphInterface tree = getTree(source);
        if (tree.getEdgeNum() < g.getOrder() - 1) {
            throw new NotAllNodesReachedException();
        }
        return tree;
    }

    private void completeVisit() {
        tree = g.create();
        resetVisited();
        for (int i = 0; i < g.getOrder(); i++) {
            if (!visited.contains(i)) {
                visit(i);
            }
        }
    }

    public boolean hasUndirectedCycle() {
        if (g instanceof UndirectedGraph) {
            return _hasUndirectedCycle();
        } else {
            return false;
        }
    }

    private boolean _hasUndirectedCycle() {
        for (int source = 0; source < g.getOrder(); source++) {
            fathers = new int[g.getOrder()];
            fathers[source] = -1;
            resetVisited();
            if(_hasUndirectedCycle(source)) {
                return true;
            }
        }
        return false;
    }

    private boolean _hasUndirectedCycle(int source) {
        visited.add(source);
        for (Integer neighbour : g.getNeighbors(source)) {
            if (!visited.contains(neighbour)) {
                fathers[neighbour] = source;
                if (_hasUndirectedCycle(neighbour)) {
                    return true;
                }
            } else if (fathers[source] != -1 && neighbour != fathers[source]) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Integer> getNodesInOrderPostVisit(int source) {
        // resetVisited();
        return getNodesInOrderPostVisit(source, new ArrayList<>());
    }


    public ArrayList<Integer> getNodesInOrderPostVisit(int source, ArrayList<Integer> ret) {
        visited.add(source);
        for (Integer neighbour : g.getNeighbors(source)) {
            if (!visited.contains(neighbour)) {
                getNodesInOrderPostVisit(neighbour, ret);
            }
        }
        ret.add(source);
        return ret;
    }

    public boolean hasDirectedCycle() {
        if (g instanceof DirectedGraph) {
            return _hasUndirectedCycle();
        } else {
            return false;
        }
    }

    public ArrayList<Integer> getDirCycle() {
        for (int source = 0; source < g.getOrder(); source++) {
            fathers = new int[g.getOrder()];
            fathers[source] = -1;
            resetVisited();
            ArrayList<Integer> ret = getDirCycle(source);
            if (ret != null) {
                return ret;
            }
        }
        return null;
    }

    private ArrayList<Integer> getDirCycle(int source) {
        visited.add(source);
        for (Integer neighbour : g.getNeighbors(source)) {
            if (!visited.contains(neighbour)) {
                fathers[neighbour] = source;
                ArrayList<Integer> ret = getDirCycle(neighbour);
                if (ret != null) {
                    return ret;
                }
            } else if (fathers[source] != -1 && neighbour != fathers[source]) {
                return getCycle(source, neighbour);
            }
        }
        return null;
    }

    private ArrayList<Integer> getCycle(int to, int from) {
        ArrayList<Integer> ret = new ArrayList<>();
        while (to != from) {
            ret.add(0, to);
            to = fathers[to];
        }
        return ret;
    }

    public boolean isConnected() {
        resetVisited();
        tree = g.create();
        visit(0);
        for (int i = 0; i < g.getOrder(); i++) {
            if (!visited.contains(i)) {
                return false;
            }
        }
        return true;
    }

    private void connectedComponents(int source, int[] cc, int count) {
        if (!visited.contains(source)) {
            visited.add(source);
            cc[source] = count;
            for (Integer neighbour : g.getNeighbors(source)) {
                connectedComponents(neighbour, cc, count);
            }
        }
    }

    public int[] connectedComponents() {
        int[] cc = new int[g.getOrder()];
        int count = 0;
        resetVisited();
        for (int i = 0; i < g.getOrder(); i++) {
            if (!visited.contains(i)) {
                connectedComponents(i, cc, count);
                count++;
            }
        }
        return cc;
    }

    public ArrayList<Integer> topologicalOrder() {
        resetVisited();
        ArrayList<Integer> ret = new ArrayList<>();
        for (int i = 0; i < g.getOrder(); i++) {
            if (!visited.contains(i)) {
                ArrayList<Integer> postVisit = getNodesInOrderPostVisit(i, new ArrayList<>());
                ret.addAll(postVisit);
            }
        }
        Collections.reverse(ret);
        return ret;
    }
}
