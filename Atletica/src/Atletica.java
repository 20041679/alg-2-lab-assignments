public class Atletica {
    private int n;
    public Atletica(int numeroDiscipline) {
        n = numeroDiscipline;
    }

    private void validate(int[] rend) {
        if (rend.length != n) {
            throw new IllegalArgumentException();
        }
    }

    public int scelta(int[] rendAtleta1, int[] rendAtleta2) {
        validate(rendAtleta1);
        validate(rendAtleta2);
        var rend1 = getMaxVal(rendAtleta1);
        var rend2 = getMaxVal(rendAtleta2);
        if (rend1 > rend2) {
            return 1;
        } else if (rend1 == rend2) {
            return 0;
        } else {
            return 2;
        }
    }

    public int getMaxVal(int[] w) {
        var a = new int[n];
        if (w.length == 0) { return 0; }
        a[0] = w[0];
        if (w.length == 1) { return a[w.length - 1]; }

        a[1] = Integer.max(w[0], w[1]);
        for (int i = 2; i < w.length; ++i) {
            a[i] = Integer.max(a[i - 1], a[i - 2] + w[i]);
        }

        return a[w.length - 1];
    }
}
