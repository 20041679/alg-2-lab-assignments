import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AtleticaTest {
    @Test
    void test2() {
        var a = new Atletica(5);
        var r1 = new int[]{8, 4, 2, 6, 3};
        var r2 = new int[]{3, 10, 7, 7, 4};
        assertEquals(2, a.scelta(r1, r2));
    }

    @Test
    void test0() {
        var a = new Atletica(5);
        var r1 = new int[]{8, 4, 2, 6, 3};
        var r2 = new int[]{3, 10, 7, 4, 4};
        assertEquals(0, a.scelta(r1, r2));
    }

    @Test
    void test1() {
        var a = new Atletica(5);
        var r1 = new int[]{8, 4, 2, 6, 3};
        var r2 = new int[]{0, 5, 1, 4, 4};
        assertEquals(1, a.scelta(r1, r2));
    }

    @Test
    void testException() {
        var a = new Atletica(5);
        var r1 = new int[]{8, 4, 2, 6, 3, 1};
        var r2 = new int[]{3, 10, 7, 7, 4};
        assertThrows(IllegalArgumentException.class, () -> a.scelta(r1, r2));
    }
}