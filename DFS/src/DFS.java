import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;

import java.util.ArrayList;
import java.util.List;

public class DFS {
    GraphInterface g;
    GraphInterface tree;
    List<Integer> visited;
    public DFS(GraphInterface g) {
        this.g = g;
    }

    int[] fathers;

    public GraphInterface getTree(int source) {
        if (source < 0 || source >= g.getOrder()) {
            throw new IllegalArgumentException("invalid source");
        }
        tree = g.create();
        visited = new ArrayList<>();
        visit(source);
        return tree;
    }

    private void visit(int source) {
        visited.add(source);
        for (Integer neighbour : g.getNeighbors(source)) {
            if (!visited.contains(neighbour)) {
                tree.addEdge(source, neighbour);
                visit(neighbour);
            }
        }
    }

    public GraphInterface getForest() {
        tree = g.create();
        visited = new ArrayList<>();
        for (int i = 0; i < g.getOrder(); i++) {
            if (!visited.contains(i)) {
                visit(i);
            }
        }
        return tree;
    }

    public GraphInterface getTreeRicoprente(int source) throws NotAllNodesReachedException {
        GraphInterface tree = getTree(source);
        if (tree.getEdgeNum() < g.getOrder() - 1) {
            throw new NotAllNodesReachedException();
        }
        return tree;
    }

    private void completeVisit() {
        tree = g.create();
        visited = new ArrayList<>();
        for (int i = 0; i < g.getOrder(); i++) {
            if (!visited.contains(i)) {
                visit(i);
            }
        }
    }
}
