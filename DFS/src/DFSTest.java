import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import static org.junit.jupiter.api.Assertions.*;

class DFSTest {
    @Test
    void testCreate() {
        GraphInterface grafo = new DirectedGraph(3);
        DFS dfsTest = new DFS(grafo);
        assertNotNull(dfsTest);
    }

    @Test
    @Timeout(value = 500)
    void testScoperti() {
        GraphInterface grafo =
                new DirectedGraph ("3;0 1;1 2;2 0");
        DFS dfsTest = new DFS(grafo);
        assertNotNull(dfsTest.getTree(0));
    }

    @Test
    void testNumeroArchi() {
        GraphInterface grafo =
                new DirectedGraph ("3;0 1;1 2;2 0");
        DFS dfsTest = new DFS(grafo);
        assertEquals(3 - 1, dfsTest.getTree(0).getEdgeNum());
    }

    @Test
    void testArchiDFS() {
        GraphInterface g =
                new UndirectedGraph("4;2 3;3 1;1 0;0 2");
        GraphInterface d = new DFS(g).getTree(2);
        assertTrue(
                (d.hasEdge(3, 1) && d.hasEdge(1, 0) && d.hasEdge(0, 2))
                || (d.hasEdge(2, 3) || d.hasEdge(3, 1) || d.hasEdge(1, 0))
        );
        assertFalse(
                (d.hasEdge(1, 0) && d.hasEdge(0, 2) && d.hasEdge(2, 3))
             || (d.hasEdge(0, 2) && d.hasEdge(2, 3) && d.hasEdge(3, 1))
        );
    }

    @Test
    void testInitAlbero() {
        GraphInterface g =
                new UndirectedGraph ("4;2 3;3 1;1 0;0 2");
        DFS d = new DFS(g);
        assertEquals(3, d.getTree(2).getEdgeNum());
        assertEquals(3, d.getTree(1).getEdgeNum());
    }

    @Test
    public void testEccezione() {
        GraphInterface g =
                new DirectedGraph ("4;2 3;3 1;1 0;0 2");
        DFS dfsTest = new DFS(g);
        assertThrows(java.lang.IllegalArgumentException.class, () -> {
            dfsTest.getTree(-3);
        });
        assertThrows(java.lang.IllegalArgumentException.class, () -> {
            dfsTest.getTree(10);
        });
        assertDoesNotThrow(() -> {
            dfsTest.getTree(0);
        },"0 e' un argomento legittimo");
    }

    @Test
    public void testTreeRicoprente() {
        GraphInterface g =
                new UndirectedGraph ("5;0 3;3 4;1 2");
        DFS dfsTest1 = new DFS(g);
        assertThrows(NotAllNodesReachedException.class, () -> dfsTest1.getTreeRicoprente(0));

        g = new UndirectedGraph("4;2 3;3 1;1 0;0 2");
        DFS dfsTest2 = new DFS(g);
        assertDoesNotThrow(() -> dfsTest2.getTreeRicoprente(0));
    }

    @Test
    public void testCompleteVisit() {
        GraphInterface g =
                new UndirectedGraph ("5;0 3;3 4;4 0;1 2");
        DFS dfsTest = new DFS(g);
        System.out.println(dfsTest.getForest());
    }
}