import it.uniupo.algoTools.MinHeap;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.UndirectedGraph;

public class AllacciamentoIdrico {
    private UndirectedGraph scavi;
    private UndirectedGraph mappa;
    private int puntoAllacciamento;
    private int costoTubo;
    private int[][] costoScavo;

    public AllacciamentoIdrico(UndirectedGraph mappa, int[][] costoScavo, int costoTubo, int puntoAllacciamento) {
        this.costoScavo = costoScavo;
        this.costoTubo = costoTubo;
        this.mappa = mappa;
        this.puntoAllacciamento = puntoAllacciamento;
    }

    public UndirectedGraph progettoProprietari() {
        boolean[] found = new boolean[mappa.getOrder()];
        int[] distance = new int[mappa.getOrder()];
        MinHeap<Edge, Integer> heap = new MinHeap<>();
        found[puntoAllacciamento] = true;
        distance[puntoAllacciamento] = 0;
        UndirectedGraph ret = (UndirectedGraph) mappa.create();
        for (Edge e : mappa.getOutEdges(puntoAllacciamento)) {
            heap.add(e, distance[puntoAllacciamento] + e.getWeight());
        }
        while (!heap.isEmpty()) {
            Edge uv = heap.extractMin();
            int u = uv.getTail();
            int v = uv.getHead();
            if (!found[v]) {
                found[v] = true;
                ret.addEdge(u, v, uv.getWeight());
                distance[v] = distance[u] + uv.getWeight();
                for (Edge vz : mappa.getOutEdges(v)) {
                    heap.add(vz, distance[v] + uv.getWeight());
                }
            }
        }
        return ret;
    }

    public UndirectedGraph progettoComune() {
        boolean[] found = new boolean[mappa.getOrder()];
        UndirectedGraph ret = (UndirectedGraph) mappa.create();
        MinHeap<Edge, Integer> heap = new MinHeap<>();
        found[puntoAllacciamento] = true;
        for (Edge uv : mappa.getOutEdges(puntoAllacciamento)) {
            int u = uv.getTail();
            int v = uv.getHead();
            heap.add(uv, costoScavo[u][v]);
        }
        while (!heap.isEmpty()) {
            Edge e = heap.extractMin();
            int u  = e.getTail();
            int v = e.getHead();
            if (!found[v]) {
                found[v] = true;
                ret.addEdge(new Edge(u, v, e.getWeight()));
                for (Edge vz : mappa.getOutEdges(v)) {
                    int z = vz.getHead();
                    heap.add(vz, costoScavo[v][z]);
                }
            }
        }
        return ret;
    }

    private int spesaComune(UndirectedGraph g) {
        return spesaComune(g, puntoAllacciamento, new boolean[g.getOrder()]);
    }

    private int spesaComune(UndirectedGraph g, int source, boolean[] found) {
        found[source] = true;
        int acc = 0;
        for (Integer neighbour : g.getNeighbors(source)) {
            if (!found[neighbour]) {
                acc += costoScavo[source][neighbour];
                acc += spesaComune(g, neighbour, found);
            }
        }
        return acc;
    }

    private int spesaProprietario(UndirectedGraph g, int villetta) {
        return spesaProprietario(g, villetta, new boolean[g.getOrder()], 0);
    }

    private int spesaProprietario(UndirectedGraph g, int source, boolean[] found, int acc) {
        found[source] = true;
        if (source == puntoAllacciamento) {
            return acc;
        }
        for (Edge e : g.getOutEdges(source)) {
            int neighbour = e.getHead();
            if (!found[neighbour]) {
                int ret = spesaProprietario(g, neighbour, found, acc + e.getWeight() * costoTubo);
                if (ret > -1) {
                    return ret;
                }
            }
        }
        return -1;
    }

    public int speseExtraComune() {
        UndirectedGraph progettoProprietari = progettoProprietari();
        UndirectedGraph progettoComune = progettoComune();
        return spesaComune(progettoProprietari) - spesaComune(progettoComune);
    }

    public int speseExtraProprietario(int villetta) {
        UndirectedGraph progettoProprietari = progettoProprietari();
        UndirectedGraph progettoComune = progettoComune();
        return spesaProprietario(progettoComune, villetta) - spesaProprietario(progettoProprietari, villetta);
    }
}