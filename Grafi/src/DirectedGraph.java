import java.util.Arrays;

public class DirectedGraph {
    private int order;
    private int[][] adjacency;

    public DirectedGraph(int order) {
        this.order = order;
        adjacency = new int[this.order][this.order];
    }

    private void checkVertex(int u) {
        if (u < 0 || u >= order) {
            throw new IndexOutOfBoundsException("outside graph bounds");
        }
    }

    private void checkEdge(int u, int v) {
        checkVertex(u);
        checkVertex(v);
    }

    public void addEdge(int u, int v) {
        checkEdge(u, v);
        adjacency[u][v] = 1;
    }

    public boolean existsEdge(int u, int v) {
        checkEdge(u, v);
        return adjacency[u][v] == 1;
    }

    public void deleteEdge(int u, int v) {
        checkEdge(u, v);
        adjacency[u][v] = 0;
    }

    public int outDegree(int u) {
        checkVertex(u);
        return Arrays.stream(adjacency[u]).sum();
    }
}
