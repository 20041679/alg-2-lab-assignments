import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DirectedGraphTest {
    @Test
    public void newDirectedGraph() {
        Assertions.assertNotNull(new DirectedGraph(3));
    }

    @Test
    public void existsEdge() {
        DirectedGraph d = new DirectedGraph(3);
        d.addEdge(0, 1);
        Assertions.assertTrue(d.existsEdge(0, 1));
        Assertions.assertFalse(d.existsEdge(1, 0));
    }

    @Test
    public void outDegree() {
        DirectedGraph d = new DirectedGraph(4);
        d.addEdge(0, 1);
        d.addEdge(1, 2);
        d.addEdge(0, 2);
        Assertions.assertEquals(2, d.outDegree(0));
        Assertions.assertEquals(0, d.outDegree(2));
    }
}
