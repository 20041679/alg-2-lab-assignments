import it.uniupo.algoTools.MinHeap;
import it.uniupo.algoTools.UnionByRank;
import it.uniupo.algoTools.UnionFind;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.UndirectedGraph;

public class Hikes {
    private UndirectedGraph rifugi;
    private boolean[][] percorribile;

    public Hikes(UndirectedGraph rifugi, boolean[][] percorribile) {
        this.rifugi = rifugi;
        this.percorribile = percorribile;
    }

    public int minDistanza(int numGite) {
        MinHeap<Edge, Integer> heap = new MinHeap<>();
        UnionFind uf = new UnionByRank(rifugi.getOrder());
        if (numGite < 2 || numGite > rifugi.getOrder()) {
            throw new IllegalArgumentException();
        }
        for (int u = 0; u < rifugi.getOrder(); ++u) {
            for (Edge uv : rifugi.getOutEdges(u)) {
                if (percorribile[uv.getTail()][uv.getHead()]) {
                    heap.add(uv, uv.getWeight());
                } else {
                    heap.add(uv, 99999);
                }
            }
        }
        while (!heap.isEmpty() && uf.getNumberOfSets() > numGite) {
            Edge uw = heap.extractMin();
            int u = uw.getTail();
            int w = uw.getHead();
            uf.union(uf.find(u), uf.find(w));
        }
        while (!heap.isEmpty()) {
            Edge uv = heap.extractMin();
            int u = uv.getTail();
            int v = uv.getHead();
            if (uf.find(u) != uf.find(v)) {
                return uv.getWeight();
            }
        }
        return -1;
    }
}
