import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class HikesTest {
    @Test
    public void testBuild() {
        UndirectedGraph rifugi = new UndirectedGraph("1");
        boolean[][] percorribile = new boolean[1][1];
        percorribile[0][0] = true;
        assertNotNull(new Hikes(rifugi, percorribile));
    }

    @Test
    public void test3() {
        UndirectedGraph rifugi = new UndirectedGraph("5; 0 1  13; 0 2 16; 0 3 19; 0 4 9; 1 2 7; 1 3 14; 1 4 22; 2 3 12; 2 4 15; 3 4 26");
        boolean[][] percorribile = new boolean[5][5];
        Arrays.fill(percorribile[0], true);
        Arrays.fill(percorribile[1], true);
        Arrays.fill(percorribile[2], true);
        Arrays.fill(percorribile[3], true);
        Arrays.fill(percorribile[4], true);
        Hikes hikes;
        hikes = new Hikes(rifugi, percorribile);
        Assertions.assertEquals(12, hikes.minDistanza(3));
        Assertions.assertEquals(13, hikes.minDistanza(2));
        percorribile[0][1] = false;
        percorribile[1][0] = false;
        percorribile[2][3] = false;
        percorribile[3][2] = false;
        hikes = new Hikes(rifugi, percorribile);
        Assertions.assertEquals(14, hikes.minDistanza(3));
        Assertions.assertEquals(15, hikes.minDistanza(2));
    }
}