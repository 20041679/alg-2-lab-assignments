import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class BFSTest {
    @Test
    void testCreate() {
        GraphInterface g = new DirectedGraph(3);
        BFS bfsTest = new BFS(g);
        assertNotNull(bfsTest);
    }

    @Test
    @Timeout(value = 500)
    void testDiscovered() {
        GraphInterface g = new DirectedGraph ("3;0 1;1 2;2 0");
        BFS bfsTest = new BFS(g);
        assertNotNull(bfsTest.getNodesInOrderOfVisit(0));
    }

    @Test
    void testVisitLength() {
        BFS bfsTest = new BFS(new DirectedGraph(1));
        assertEquals(1, bfsTest.getNodesInOrderOfVisit(0).size());

        bfsTest = new BFS(new DirectedGraph("2; 0 1"));
        assertEquals(2, bfsTest.getNodesInOrderOfVisit(0).size());

        bfsTest = new BFS(new UndirectedGraph("4;0 2;0 1;2 3;1 3"));
        assertEquals(4,bfsTest.getNodesInOrderOfVisit(2).size());
    }

    @Test
    void testBFSOrder() {
        BFS bfsTest = new BFS( new UndirectedGraph ("4;0 2;0 1;2 3;1 3"));
        assertTrue(bfsTest.getNodesInOrderOfVisit(2).get(2) == 0 ||
                bfsTest.getNodesInOrderOfVisit(2).get(2) == 3);
        assertNotEquals(1, (int) bfsTest.getNodesInOrderOfVisit(2).get(2));
    }

    @Test
    void testInitNumberOfVisited() {
        GraphInterface g =
                new UndirectedGraph("4;0 2;0 1;2 3;1 3");
        BFS bfsTest = new BFS(g);
        int n = bfsTest.getNodesInOrderOfVisit(0).size();
        assertEquals(4, n);
        n = bfsTest.getNodesInOrderOfVisit(2).size();
        assertEquals(4, n);
    }

    @Test
    void testInitNodesOrder() {
        GraphInterface g =
                new UndirectedGraph("4;0 2;0 1;2 3;1 3");
        BFS bfsTest = new BFS(g);
        assertNotEquals(1, bfsTest.getNodesInOrderOfVisit(2).get(2));
        assertNotEquals(2, bfsTest.getNodesInOrderOfVisit(1).get(2));
        assertNotEquals(3, bfsTest.getNodesInOrderOfVisit(0).get(2));
    }

    @Test
    void testDistance() {
        int[] distances = (new BFS(new UndirectedGraph(1))).getDistance(0);
        assertEquals(0, distances[0]);

        distances = (new BFS(new UndirectedGraph("2; 0 1"))).getDistance(0);
        assertEquals(0, distances[0]);

        distances = (new BFS(new UndirectedGraph("4;0 2;0 1;2 3;1 3"))).getDistance(2);
        assertEquals(0, distances[2]);
        assertEquals(1, distances[0]);
        assertEquals(1, distances[3]);
        assertEquals(2, distances[1]);
    }
}