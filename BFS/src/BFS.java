import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class BFS {
    private GraphInterface g;

    public BFS(GraphInterface g) {
        this.g = g;
    }

    public ArrayList<Integer> getNodesInOrderOfVisit(int source) {
        Queue<Integer> queue = new LinkedList<>();
        boolean[] discovered = new boolean[g.getOrder()];

        ArrayList<Integer> ret = new ArrayList<>();
        queue.add(source);
        discovered[source] = true;
        while (!queue.isEmpty()) {
            Integer u = queue.remove();
            ret.add(u);
            for (Integer v : g.getNeighbors(u)) {
                if (!discovered[v]) {
                    queue.add(v);
                    discovered[v] = true;
                }
            }
        }
        return ret;
    }

    public int[] getDistance(int source) {
        Queue<Integer> queue = new LinkedList<>();
        boolean[] discovered = new boolean[g.getOrder()];
        int[] distances = new int[g.getOrder()];
        distances[source] = 0;

        queue.add(source);
        discovered[source] = true;
        while (!queue.isEmpty()) {
            Integer u = queue.remove();
            for (Integer v : g.getNeighbors(u)) {
                if (!discovered[v]) {
                    queue.add(v);
                    discovered[v] = true;
                    distances[v] = distances[u] + 1;
                }
            }
        }

        return distances;
    }

}
