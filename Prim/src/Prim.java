import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;
import it.uniupo.algoTools.MinHeap;

public class Prim {
    UndirectedGraph g;
    int cost;

    Prim(GraphInterface g) {
        this.g = (UndirectedGraph) g;
    }

    private UndirectedGraph visit(int source) {
        cost = 0;
        boolean[] visited = new boolean[g.getOrder()];
        visited[source] = true;
        MinHeap<Edge, Integer> heap = new MinHeap<>();
        UndirectedGraph tree = (UndirectedGraph) g.create();
        for (Edge e : g.getOutEdges(source)) {
            heap.add(e, e.getWeight());
        }
        while (!heap.isEmpty()) {
            Edge uv = heap.extractMin();
            int v = uv.getHead();
            if (!visited[v]) {
                visited[v] = true;
                tree.addEdge(uv);
                cost += uv.getWeight();
                for (Edge vz : g.getOutEdges(v)) {
                    int z = vz.getHead();
                    if (!visited[z]) {
                        heap.add(vz, vz.getWeight());
                    }
                }
            }
        }
        return tree;
    }

    public UndirectedGraph getMST() {
        return visit(0);
    }

    public int getCost() {
        visit(0);
        return this.cost;
    }
}
