import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrimTest {
    @Test
    void getMst() {
        UndirectedGraph g;
        g = new UndirectedGraph("5; 0 2 2; 0 1 1; 1 2 3; 1 4 5; 2 3 7; 2 4 6; 3 4 4");
        Prim prim = new Prim(g);
        Dijkstra dijkstra = new Dijkstra(g);
        Assertions.assertNotEquals(prim.getCost(), dijkstra.distance(0, 3));
    }
}