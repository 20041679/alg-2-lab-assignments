import it.uniupo.algoTools.MinHeap;
import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.Graph;
import it.uniupo.graphLib.GraphInterface;

import java.util.Arrays;

public class Dijkstra {
    int[] d;
    boolean[] visited;
    MinHeap<Edge, Integer> h;
    GraphInterface g;

    public Dijkstra(GraphInterface collegamenti) {
        g = collegamenti;
        d = new int[collegamenti.getOrder()];
        h = new MinHeap<>();
        visited = new boolean[g.getOrder()];
    }

    private void init(int source) {
        Arrays.fill(d, -1);
        d[source] = 0;
        for (Edge su : g.getOutEdges(source)) {
            h.add(su, d[source] + su.getWeight());
        }
        visited[source] = true;
    }

    private void validate(int partenza, int destinazione) {
        if (partenza < 0 || destinazione < 0 || partenza > g.getOrder() || destinazione > g.getOrder()) {
            throw new IllegalArgumentException();
        }
    }

    public int distance(int partenza, int destinazione) {
        validate(partenza, destinazione);
        init(partenza);
        while (!h.isEmpty()) {
            Edge uw = h.extractMin();
            int u = uw.getTail();
            int w = uw.getHead();
            if (!visited[w]) {
                visited[w] = true;
                d[w] = d[u] + uw.getWeight();
                for (Edge wz : g.getOutEdges(w)) {
                    int z = wz.getHead();
                    if (!visited[z]) {
                        h.add(wz, d[w] + wz.getWeight());
                    }
                }
            }
        }
        return d[destinazione];
    }
}
