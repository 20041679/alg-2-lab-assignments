import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ClusteringTest {
    @Test
    public void test4Clusters() {
        UndirectedGraph g = new UndirectedGraph("4; 0 1 2; 1 3 7; 3 2 4; 2 0 1");
        Clustering clustering = new Clustering(g, 4);

        Assertions.assertFalse(clustering.sameCluster(0, 1));
        Assertions.assertFalse(clustering.sameCluster(0, 2));
        Assertions.assertFalse(clustering.sameCluster(0, 3));

        Assertions.assertFalse(clustering.sameCluster(1, 2));
        Assertions.assertFalse(clustering.sameCluster(1, 3));

        Assertions.assertFalse(clustering.sameCluster(2, 3));
    }

    public void test3Clusters() {

    }
}