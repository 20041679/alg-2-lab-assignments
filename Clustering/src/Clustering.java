import it.uniupo.algoTools.MinHeap;
import it.uniupo.algoTools.UnionByRank;
import it.uniupo.algoTools.UnionFind;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.UndirectedGraph;

public class Clustering {
    private UndirectedGraph g;
    private int numCluster;
    private UnionFind uf;
    private MinHeap<Edge, Integer> heap;

    public Clustering(UndirectedGraph g, int numCluster) {
        this.g = g;
        this.numCluster = numCluster;
        uf = new UnionByRank(g.getOrder());
        heap = new MinHeap<>();
    }

    public Integer spaziamento() {
        for (int u = 0; u < g.getOrder(); ++u) {
            for (Edge uv : g.getOutEdges(u)) {
                heap.add(uv, uv.getWeight());
            }
        }
        while (!heap.isEmpty() && uf.getNumberOfSets() > numCluster) {
            Edge uw = heap.extractMin();
            int u = uw.getTail();
            int w = uw.getHead();
            uf.union(uf.find(u), uf.find(w));
        }
        while (!heap.isEmpty()) {
            Edge uv = heap.extractMin();
            int u = uv.getTail();
            int v = uv.getHead();
            if (!sameCluster(u, v)) {
                return uv.getWeight();
            }
        }
        return null;
    }

    public boolean sameCluster(int a, int b) {
        return uf.find(a) == uf.find(b);
    }
}
