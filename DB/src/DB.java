import it.uniupo.algoTools.MaxHeap;

import java.util.Arrays;

public class DB {
    private int[] dim;
    private double[] time;
    private int[] toCompute;

    public DB(int[] dim, double[] time) {
        this.dim = dim;
        this.time = time;
        toCompute = dim.clone();
    }

    public double timeToRebuild(int memSpace) {
        if (memSpace < 0) {
            throw new IllegalArgumentException();
        }

        MaxHeap<Integer, Double> heap = new MaxHeap<>();
        for (int i = 0; i < time.length; i++) {
            heap.add(i, time[i] / dim[i]);
        }

        double ret = 0;
        while (memSpace > 0 && !heap.isEmpty()) {
            int m = heap.extractMax();
            int stored = memSpace >= dim[m] ? dim[m] : memSpace;
            // either whole or partial databases,
            // if `stored` != `dim[m]` partial database is taken, hence last iteration
            ret += (dim[m] - stored) * (time[m] / dim[m]);
            memSpace -= stored;
        }
        while (!heap.isEmpty()) {
            int m = heap.extractMax();
            // add time for whole database, no more partial databases
            ret += time[m];
        }
        return ret;
    }
}
